# README #

### What is this repository for? ###

* This repository contains a few quickstart examples to show how to use the testcases from the jta-xaresource-compatibility-testcases project.
* The core-quickstarts are maintained together with the jta-xaresource-compatibility-testcases project.
* In this repository you should only have qickstarts that reference a release version of the jta-xaresource-compatibility-testcases project.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

Start by following the set-up instructions from the jta-xaresource-compatibility-testcases project project. Then in the project workspace directory, e.g. 'jta-xaresource-compatibility', do the following.

Clone the repository

	> git clone https://bitbucket.org/jta-xaresource-compatibility/quickstarts.git

and initialize it as an Eclipse project:

	> cd quickstarts
	> mvn eclipse:eclipse

Then open Eclipse with the project workspace directory:

	> cd ../..
	> eclipse -data jta-xaresource-compatibility

And finally import the project into Eclipse via:

	>File>Import>General>Existing Projects Into Workspace

Click 'Browse' and then 'Ok' and select the quickstarts that you're interested in.

Finally you should have everything set-up to start working on the quickstart project in Eclipse.

### Contribution guidelines ###

The [Collective Code Construction Contract](http://rfc.zeromq.org/spec:16) (C4) is an evolution of the github.com Fork + Pull Model, aimed at providing an optimal collaboration model for free software projects. C4 is derived from the ZeroMQ contribution policy of early 2012.

### Who do I talk to? ###

* This repository is a team repository. Talk to one of its admins if you have any questions you would like to ask.

